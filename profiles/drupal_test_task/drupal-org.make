; drupal_test_task make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.x-dev"
projects[admin_menu][subdir] = "contrib"

projects[adminimal_admin_menu][version] = "1.6"
projects[adminimal_admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.0"
projects[module_filter][subdir] = "contrib"

projects[ctools][version] = "1.9"
projects[ctools][subdir] = "contrib"

projects[devel][version] = "1.5"
projects[devel][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[features][version] = "2.6"
projects[features][subdir] = "contrib"

projects[test_task][version] = "1.0-beta2"
projects[test_task][subdir] = "contrib"

projects[backup_migrate][version] = "3.1"
projects[backup_migrate][subdir] = "contrib"

projects[strongarm][version] = "2.x-dev"
projects[strongarm][subdir] = "contrib"

projects[jquery_update][version] = "3.0-alpha3"
projects[jquery_update][subdir] = "contrib"

projects[views][version] = "3.11"
projects[views][subdir] = "contrib"

; +++++ Themes +++++

; adminimal_theme
projects[adminimal_theme][type] = "theme"
projects[adminimal_theme][version] = "1.23"
projects[adminimal_theme][subdir] = "contrib"

; bootstrap
projects[bootstrap][type] = "theme"
projects[bootstrap][version] = "3.x-dev"
projects[bootstrap][subdir] = "contrib"

