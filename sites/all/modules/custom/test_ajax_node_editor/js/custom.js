(function ($) {
    Drupal.behaviors.testAjaxCustom = {
        attach: function (context, settings) {

            var $modal = $('#modal');

            var ajaxNodeEdit = function(e) {
                e.preventDefault();
                $('.modal-custom-content', context).load(this.href, function() {
                    //Drupal.ajax[pots] = new Drupal.ajax(false, '#something-node-form', {url: '/system/ajax'});
                    Drupal.attachBehaviors('#something-node-form');
                    $('#something-node-form').ajaxForm();
                });
            }

            $('.ajaxed', context).click( function(e) {
                e.preventDefault();
                $modal.find('.modal-custom-content').html('<p>Loading...</p>')
                $modal.modal('show');
                $.get(this.href, function(data) {
                    $('.modal-custom-content', context).html(data);
                    //$modal.modal('handleUpdate');

                    $('.ajax-node-edit').click(ajaxNodeEdit)
                })
            });


            /*$('#remote-content-wrapper').once('remote-content-wrapper', function() {

                var base = $(this).attr('id');

                var element_settings = {
                    url: 'http://' + window.location.hostname +  settings.basePath + settings.pathPrefix + 'ajax/remote',
                    event: 'click',
                    progress: {
                        type: 'throbber'
                    }
                };
                Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
                $(this).click();
            });*/

            $(document).on("ajaxComplete", function(data, pots){
                //console.log(data);
                //console.log(pots);
            });

        }
    }
})(jQuery);