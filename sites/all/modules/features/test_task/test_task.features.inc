<?php
/**
 * @file
 * test_task.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function test_task_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function test_task_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function test_task_image_default_styles() {
  $styles = array();

  // Exported image style: somestyle_250x200.
  $styles['somestyle_250x200'] = array(
    'label' => 'SomeStyle (250x200)',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 250,
          'height' => 200,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function test_task_node_info() {
  $items = array(
    'something' => array(
      'name' => t('Something'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
